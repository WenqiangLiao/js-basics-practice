function flatArray(arr) {
  // Need to be implemented
  return arr.flat();
}

function aggregateArray(arr) {
  // Need to be implemented
  let result = arr.reduce((rows, item) => rows.push([item * 2])&&rows, []);
  return result;
}

function getEnumerableProperties(obj) {
  // Need to be implemented
  let result = [];
  for(let [key, value] of Object.entries(obj)){
      result.push(key);
  }
  return result;
}

function removeDuplicateItems(arr) {
  // Need to be implemented
  return arr.filter((item, index) => {
    return arr.indexOf(item) === index;
  })
}

function removeDuplicateChar(str) {
  return ([...str].filter((item, index) => {
    return [...str].indexOf(item) === index;
  })).join("");
}

function addItemToSet(set, item) {
  // Need to be implemented
  return set.add(item);
}

function removeItemToSet(set, item) {
  // Need to be implemented
  let result = [];
  set.forEach(element => {
    if(element != item)
      result.push(element);
  });
  return new Set(result);
}

function countItems(arr) {
  // Need to be implemented
  let result = {};
  let map = [];
  arr.forEach(element => {
    if(!result[element]){
      result[element] = 1;
    }else{
      result[element] += 1;
    }
    }
  );
  for(let [key, value] of Object.entries(result)){
      map.push([parseInt(key), value]);
  }
  return new Map(map);
}

export {
  flatArray,
  aggregateArray,
  getEnumerableProperties,
  removeDuplicateItems,
  removeDuplicateChar,
  addItemToSet,
  removeItemToSet,
  countItems
};
