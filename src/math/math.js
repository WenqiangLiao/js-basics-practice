function upwardNumber(num) {
  // Need to be implemented
  return Math.ceil(num);
}

function downwardNumber(num) {
  // Need to be implemented
  return Math.floor(num);
}

function roundNumber(num) {
  // Need to be implemented
  return Math.round(num);
}

function random(startNum, endNum) {
  // Need to be implemented
  return Math.random()*(endNum - startNum) + startNum;
}

function minNumber(arr) {
  // Need to be implemented
  let min = arr.reduce((pre, cur) => {
    return cur < pre? cur:pre;
  });
  return min;
}

function maxNumber(arr) {
  // Need to be implemented
  let max = arr.reduce((pre, cur) => {
    return cur>pre? cur:pre;
  });
  return max;
}

export {
  upwardNumber,
  downwardNumber,
  roundNumber,
  random,
  maxNumber,
  minNumber
};
