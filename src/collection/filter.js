function getAllEvens(collection) {
  let newArr = collection.filter((item) => {
    return item % 2 === 0;
  });
  return newArr;
}

function getAllIncrementEvens(start, end) {
  // Need to be implemented
  let arr = [];
  for(start;start<=end;start++){
    if(start % 2 === 0){
      arr.push(start);
    }
  }
  return arr;
}

function getIntersectionOfcollections(collection1, collection2) {
  // Need to be implemented
  let arr = [];
  for(let i=0; i < collection1.length; i++){
    for(let j=0; j<collection2.length; j++){
      if(collection2[j] === collection1[i]){
        arr.push(collection1[i]);
      }
    }
  }
  return arr;
}

function getUnionOfcollections(collection1, collection2) {
  // Need to be implemented
  let newArr = collection1.concat(collection2);
  let resultArr = newArr.filter((item, index, newArr) => {
    return newArr.indexOf(item) === index;
  })

  return resultArr;
}

function countItems(collection) {
  // Need to be implemented
  let result = {};
  for(let i=0; i<collection.length; i++){
    if(!result[collection[i]]){
      result[collection[i]] = 1;
    }else{
      result[collection[i]] += 1;
    }
 }
 return result;
}

export {
  getAllEvens,
  getAllIncrementEvens,
  getIntersectionOfcollections,
  getUnionOfcollections,
  countItems
};
