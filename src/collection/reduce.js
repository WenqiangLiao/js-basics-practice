function getMaxNumber(collction) {
  // Need to be implemented
  let max = collction.reduce(function(pre, cur) {
    return pre > cur ? pre:cur;
  });
  return max;
}

function isSameCollection(collction1, collction2) {
  // Need to be implemented
  if(collction1.length != collction2.length){
    return false;
  }else{
    for(let i=0 ;i<collction1.length; i++){
      if(collction1[i] != collction2[i]){
        return false;
      }
    }
    return true;
  }
}

function sum(collction) {
  // Need to be implemented
  return collction.reduce((pre, cur) => {
    return pre + cur;
  });
}

function computeAverage(collction) {
  // Need to be implemented
  return (collction.reduce((pre, cur) => {
    return pre + cur;
  }))/collction.length;
}

function lastEven(collction) {
  // Need to be implemented
  let lastEven = collction.reduce(function(pre, cur) {
    return cur%2===0 ? cur:pre;
  });
  return lastEven;
}

export { getMaxNumber, isSameCollection, computeAverage, sum, lastEven };
