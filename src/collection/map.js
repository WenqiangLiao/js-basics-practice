function doubleItem(collection) {
  // Need to be implemented
  return collection.map((item) => {
    return item * 2;
  });
}

function doubleEvenItem(collection) {
  // Need to be implemented
  let newArr = collection.filter((item) => {
    return item%2===0;
  });
  return newArr.map((item) => {
    return item * 2;
  });
}

function covertToCharArray(collection) {
  // Need to be implemented
  return collection.map((item) => {
    return String.fromCharCode(item + 96);
  });
}

function getOneClassScoreByASC(collection) {
  // Need to be implemented
  return ((collection.filter((item) => {
      return item.class === 1;
  })).sort((a, b) => {
    return a.score - b.score;
  })).map((item) => {
    return item.score;
  });
}

export { doubleItem, doubleEvenItem, covertToCharArray, getOneClassScoreByASC };
