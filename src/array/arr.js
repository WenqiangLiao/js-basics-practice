function joinArrays(arr1, arr2) {
  const output = arr1.concat(arr2);
  return output;
}

function checkAdult(arr, age) {
  for(let i=0; i<arr.length; i++){
    if(arr[i] < age){
      return false;
    }
  }
  return true;
}


function findAdult(arr, age) {
  for(let i=0; i<arr.length; i++){
    if(arr[i] >= age){
      return arr[i];
    }
  }
}

function convertArrToStr(arr, str) {
  // Need to be implemented
  let result;
  result = arr.join(str);
  return result;
}

function removeLastEle(arr) {
  // Need to be implemented
  arr.pop();
  return arr;
}

function addNewItem(arr, item) {
  // Need to be implemented
  arr.push(item);
  return arr;
}

function removeFirstItem(arr) {
  // Need to be implemented
  arr.shift();
  return arr;
}

function addNewItemToBeginArr(arr, item) {
  // Need to be implemented
  arr.unshift(item);
  return arr;
}

function reverseOrder(arr) {
  // Need to be implemented
  arr.reverse();
  return arr;
}

function selectElements(arr, start, end) {
  // Need to be implemented
  let result = arr.slice(start, end);
  return result;
}

function addItemsToArray(arr, index, howmany, item) {
  // Need to be implemented
  arr.splice(index, howmany, item);
  return arr;
}

function sortASC(arr) {
  // Need to be implemented
  function cmp(a, b){return a-b;}
  arr.sort(cmp);
  return arr;
}

function sortDESC(arr) {
  // Need to be implemented
  function cmp(a, b){return b-a;}
  arr.sort(cmp);
  return arr;
}

export {
  joinArrays,
  checkAdult,
  findAdult,
  convertArrToStr,
  removeLastEle,
  addNewItem,
  removeFirstItem,
  addNewItemToBeginArr,
  reverseOrder,
  selectElements,
  addItemsToArray,
  sortASC,
  sortDESC
};
